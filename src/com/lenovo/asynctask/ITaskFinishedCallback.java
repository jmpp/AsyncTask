/**
 * Project:      AsyncTask
 * FileName:     ITaskFinishedCallback.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年1月12日 下午3:13:52
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask;

/**
 * 类 ITaskFinishedCallback 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年1月12日下午3:13:52
 */
public interface ITaskFinishedCallback {
    public void onTaskFinished(ITaskReference taskRef);
}
