/**
 * Project:      AsyncTask
 * FileName:     CommonTaskReference.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年1月16日 上午10:23:14
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask.impl;

import java.util.Date;
import java.util.Properties;

import com.lenovo.asynctask.Task;
import com.lenovo.asynctask.TaskState;

/**
 * 类 TaskReferenceImpl 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年1月16日上午10:23:14
 */
public class CommonTaskReference extends TaskReferenceBase {

    private Task _task;
    private TaskState   _state;
    private String      _msg;
    private int         _progress;
    private Date        _startedTime;
    private Date        _finishedTime;
    private Object      _result;
    private String      _flag;

    private boolean     _canModify = false;
    private Properties  _props;

    /**
     * @param taskId
     * @param canModify
     */
    public CommonTaskReference(Task task, boolean canModify) {
        this._task = task;
        this._canModify = canModify;
    }

    /**
     * @author ligh4 2015年1月16日下午1:32:54
     */
    @Override
    public boolean isFinished() {
        if (_state == TaskState.success || _state == TaskState.failed
                || _state == TaskState.timeout) {
            return true;
        }
        return false;
    }

    /**
     * @author ligh4 2015年1月19日下午3:31:50
     */
    @Override
    public String getProperty(String key) {
        if (_props != null) {
            return _props.getProperty(key);
        }
        return null;
    }

    /**
     * @author ligh4 2015年1月19日下午3:31:50
     */
    @Override
    public void setProperty(String key, String value) {
        if (_canModify) {
            if (_props == null) {
                _props = new Properties();
            }
            _props.setProperty(key, value);
        }

    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public String getType() {
        return _task.getType();
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public String getSubType() {
        return _task.getSubType();
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public String getId() {
        return _task.getId();
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public TaskState getState() {
        return _state;
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public int getTimeoutMillis() {
        return _task.getTimeoutMillions();
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public boolean isNeverTimeout() {
        return _task.isNeverTimeOut();
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public Date getStartedTime() {
        return _startedTime;
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public void setState(TaskState state, String msg) {
        if (_canModify) {
            if (_state == null) {
                _startedTime = new Date();
            }
            _state = state;
            _msg = msg;

            if (isFinished()) {
                _finishedTime = new Date();
            }
        }

    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public void setProgress(int progress) {
        if (_canModify) {
            _progress = progress;
        }
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public Object getInput() {
        return _task.getInput();
    }

    /**
     * @author ligh4 2015年1月20日上午11:17:43
     */
    @Override
    public void setResult(Object result) {
        if (_canModify) {
            _result = result;
        }

    }

    /**
     * @author ligh4 2015年1月20日下午3:51:58
     */
    @Override
    public Date getFinishedTime() {
        return _finishedTime;
    }

    /**
     * @author ligh4 2015年1月20日下午3:51:58
     */
    @Override
    public String getFlag() {
        return _flag;
    }

    /**
     * @author ligh4 2015年1月20日下午3:51:58
     */
    @Override
    public void setFlag(String flag) {
        if (_canModify) {
            _flag = flag;
        }

    }

    /**
     * @author ligh4 2015年1月20日下午4:14:27
     */
    @Override
    public String getMsg() {
        return _msg;
    }

    /**
     * @author ligh4 2015年1月20日下午4:25:32
     */
    @Override
    public int getProgress() {
        return _progress;
    }

    /**
     * @author ligh4 2015年1月20日下午4:25:32
     */
    @Override
    public Object getResult() {
        return _result;
    }

    /**
     * @author ligh4 2015年1月21日上午9:26:05
     */
    @Override
    public boolean isRecoveredTask() {
        return false;
    }

    /**
     * @author ligh4 2015年1月21日下午5:47:30
     */
    @Override
    public boolean isPresistenceTask() {
        return false;
    }

}
