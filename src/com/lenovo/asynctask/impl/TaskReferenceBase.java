/**
 * Project:      AsyncTask
 * FileName:     TaskReferenceBase.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年1月22日 下午2:12:49
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask.impl;

import com.lenovo.asynctask.ITaskReferenceInternal;

/**
 * 类 TaskReferenceBase 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年1月22日下午2:12:49
 */
public abstract class TaskReferenceBase implements ITaskReferenceInternal {

    private boolean isFinishedEventFired = false;

    public boolean isFinishedEventFired() {
        return isFinishedEventFired;
    }

    public void setFinishedEventFired() {
        isFinishedEventFired = true;
    }

    /**
     * @author ligh4 2015年1月22日下午3:56:51
     */
    @Override
    public void waitForTask() {
        while (!isFinished()) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {

            }
        }
    }

}
